# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

[[ $UID = 0 ]] && ZSH_DISABLE_COMPFIX=true
# Path to your oh-my-zsh installation.
if [ "$(lsb_release -is)" = "EndeavourOS" ]; then
  export ZSH=/usr/share/oh-my-zsh/
elif [ "$(lsb_release -is)" = "ManjaroLinux" ]; then
  export ZSH=/usr/share/oh-my-zsh/
elif [ "$(lsb_release -is)" = "Ubuntu" ]; then
  export ZSH=$HOME/.oh-my-zsh
elif [ "$(lsb_release -is)" = "Fedora" ]; then
  export ZSH=$HOME/.oh-my-zsh
fi
ZSH_THEME="powerlevel10k"

plugins=(git pass colored-man-pages colorize command-not-found vagrant tig ruby archlinux docker docker-compose rvm)

source $ZSH/oh-my-zsh.sh

# User configuration
source $HOME/.aliases

export MANPATH="/usr/local/man:$MANPATH"
export LANG=en_US.UTF-8
export QT_QPA_PLATFORMTHEME=gtk2
export PATH=/usr/local/bin:$PATH
export PASSWORD_STORE_DIR="$HOME/Code/git_repos/pass"

fpath=(~/.zsh/completion $fpath)
autoload -Uz compinit && compinit -i
autoload -U bashcompinit
bashcompinit
zstyle ':completion:*:commands' rehash 1
zstyle ':completion:*' completer _oldlist _expand _complete _files _ignored
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' use-cache yes
zstyle ':completion:*' menu select=5

if [ "$(lsb_release -is)" = "EndeavourOS" ]; then
  [[ -f  /usr/share/fzf/key-bindings.zsh ]] && source /usr/share/fzf/key-bindings.zsh
  [[ -f /usr/share/fzf/completion.zsh ]]    && source /usr/share/fzf/completion.zsh
elif [ "$(lsb_release -is)" = "ManjaroLinux" ]; then
  [[ -f  /usr/share/fzf/key-bindings.zsh ]] && source /usr/share/fzf/key-bindings.zsh
  [[ -f /usr/share/fzf/completion.zsh ]]    && source /usr/share/fzf/completion.zsh
elif [ "$(lsb_release -is)" = "Ubuntu" ]; then
  [[ -f  /usr/share/doc/fzf/examples/key-bindings.zsh ]]  && source /usr/share/doc/fzf/examples/key-bindings.zsh
  [[ -f /usr/share/zsh/vendor-completions/_fzf ]]         && source /usr/share/zsh/vendor-completions/_fzf
elif [ "$(lsb_release -is)" = "Fedora" ]; then
  [[ -f /usr/share/fzf/shell/key-bindings.zsh ]]  && source /usr/share/fzf/shell/key-bindings.zsh
  [[ -f /usr/share/zsh/site-functions/fzf ]]      && source /usr/share/zsh/site-functions/fzf 
fi

export FZF_DEFAULT_COMMAND='ag --hidden --ignore .git -g ""'
#export FZF_DEFAULT_OPTS="--multi  --reverse --preview '[[ \$(file --mime {}) =~ binary ]] && echo {} is a binary file || (bat --style=numbers --color=always {} || cat {} ) 2> /dev/null | head -50'"
export FZF_DEFAULT_OPTS="--multi  --reverse"

wttr() {
  curl wttr.in/$1
}

update-dotfiles() {
  DOTDIR="$HOME/.dotfiles"
  OLDDIR=$(pwd)

  if [[ -d $DOTDIR ]]; then
    cd $DOTDIR

    echo -n "Update ${DOTDIR}... "
    git fetch --quiet --all
    git reset --quiet --hard origin/master
    echo "Done"

    cd $OLDDIR
  else
    echo "Abort."
  fi
}

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

export ANSIBLE_FORCE_COLOR=true

# SSH completion
h=()
if [[ -r ~/.ssh/config ]]; then
  h=($h ${${${(@M)${(f)"$(cat ~/.ssh/config)"}:#Host *}#Host }:#*[*?]*})
fi
if [[ -r ~/.ssh/known_hosts ]]; then
  h=($h ${${${(f)"$(cat ~/.ssh/known_hosts{,2} || true)"}%%\ *}%%,*}) 2>/dev/null
fi
if [[ $#h -gt 0 ]]; then
  zstyle ':completion:*:ssh:*' hosts $h
  zstyle ':completion:*:slogin:*' hosts $h
fi

eval "$(_MOLECULE_COMPLETE=zsh_source molecule)"


# Ansible argcomplete
eval "$(register-python-argcomplete ansible)"
eval "$(register-python-argcomplete ansible-config)"
eval "$(register-python-argcomplete ansible-console)"
eval "$(register-python-argcomplete ansible-doc)"
eval "$(register-python-argcomplete ansible-galaxy)"
eval "$(register-python-argcomplete ansible-inventory)"
eval "$(register-python-argcomplete ansible-playbook)"
eval "$(register-python-argcomplete ansible-pull)"
eval "$(register-python-argcomplete ansible-vault)"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
export PATH="$GEM_HOME/bin:$PATH"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

export PATH="$HOME/.poetry/bin:$PATH"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
