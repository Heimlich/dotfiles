# Dotfiles

For managing the configs GNU Stow is used. Every directory represents a
configuration path relative to $HOME.
To install a single configuration(e.g. zsh; stow needs to be installed) just do

```
$ cd Dotfiles
$ stow -t ~ zsh
$ stow -t ~ nvim
```

For stowing the system files use:
```
sudo stow -t / -v system_files
```

or for all configs

```
make
```

For further reading see `man stow`.


## System Base Init
Make Sure the dirs exist to prevent from linking complete dirs instead of files.
```bash
stow -t ~ zsh
stow -t ~ nvim

```
