" Dep powerline-fonts, nerd-fonts-jetbrains-mono
"set viminfo+=n~/.vim/viminfo
"set directory=$HOME/.vim/swapfiles/ "swapfile dir
"set undodir=$HOME/.vim/undo " where to save undo histories

set viminfo+=n~/.local/share/nvim/site/viminfo
set directory=$HOME/.local/share/nvim/site/swapfiles/ "swapfile dir
set undodir=$HOME/.local/share/nvim/site/undo " where to save undo histories

call plug#begin('~/.local/share/site/nvim/plugged')
  "Basic Editor
  Plug 'preservim/nerdtree'
  Plug 'junegunn/fzf'
  Plug 'junegunn/fzf.vim'
  " Plug 'ctrlpvim/ctrlp.vim'
  Plug 'easymotion/vim-easymotion'
  Plug 'godlygeek/tabular'
  Plug 'tpope/vim-surround'
  Plug 'airblade/vim-gitgutter'
  "Plug 'w0rp/ale'
  Plug 'tpope/vim-commentary'
  Plug 'jiangmiao/auto-pairs'
  " Tab completion
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  "Plug 'ervandew/supertab'
  "Plug 'craigemery/vim-autotag'
  " Plug 'SirVer/ultisnips'
  " Plug 'honza/vim-snippets'
  " Plug 'ycm-core/YouCompleteMe'
  "Styling
  Plug 'flazz/vim-colorschemes'
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  Plug 'ryanoasis/vim-devicons'
  "Language Specific
  Plug 'sheerun/vim-polyglot'
  Plug 'ngmy/vim-rubocop'
  Plug 'plasticboy/vim-markdown'
  " Plug 'pearofducks/ansible-vim'
  Plug 'tpope/vim-haml'
  Plug 'ap/vim-css-color'
call plug#end()

syntax enable
"     ### Styling ###
let g:solarized_termcolors=256
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:gitgutter_override_sign_column_highlight = 1
autocmd ColorScheme * highlight SignColumn guibg=bg
autocmd ColorScheme * highlight SignColumn ctermbg=bg
" Set light color sheme
" set: set background=light
" comment hi Pmenu
set background=dark
" set background=light
"colorscheme solarized8_dark
"colorscheme molokai
colorscheme solarized
hi Pmenu ctermbg=black ctermfg=white




set number ruler cursorline
set cursorcolumn
" set menu for completion in e.g. tabedit
set wildmode=longest,list,full
set wildmenu

" activates highliting for / searches
set ignorecase hlsearch
set incsearch
nmap <leader>h :nohlsearch<cr>
" ----------------------

"   ### Editor Basic Conf ###
"  Faster update interval used by e.g. gitgutter
set updatetime=250
" native splitt opening
set splitbelow
set splitright

set undofile                " Save undos after file closes
set undolevels=1000         " How many undos
set undoreload=10000        " number of lines to save for undo

" cTags
" set tags=./tags
set tags=tags;/

set mouse=a
set clipboard+=unnamedplus
" " " Copy to clipboard
" vnoremap  <leader>y  "+y
" nnoremap  <leader>Y  "+yg_
" nnoremap  <leader>y  "+y
" " " Paste from clipboard
" nnoremap <leader>p "+p
" nnoremap <leader>P "+P
" vnoremap <leader>p "+p
" vnoremap <leader>P "+P
" Copy to to X11 marked text clipboard on double click
nnoremap <silent> <2-LeftMouse> :let @/='\V\<'.escape(expand('<cword>'), '\').'\>'<cr>:set hls<cr>:let @*=expand('<cword>')<cr>

" Move lines in all modes
nnoremap <C-j> :m .+1<CR>==
nnoremap <C-k> :m .-2<CR>==
inoremap <C-j> <Esc>:m .+1<CR>==gi
inoremap <C-k> <Esc>:m .-2<CR>==gi
vnoremap <C-j> :m '>+1<CR>gv=gv
vnoremap <C-k> :m '<-2<CR>gv=gv
" Better split navigation
nnoremap <S-H> <C-W><C-H>
nnoremap <S-j> <C-W><C-J>
nnoremap <S-k> <C-W><C-K>
nnoremap <S-l> <C-W><C-L>
" resize current buffer by +/- 5
nnoremap <Up> :resize +2<cr>
nnoremap <Down> :resize -2<cr>
nnoremap <Left> :vertical resize +2<cr>
nnoremap <Right> :vertical resize -2<cr>
" use jj intead of esc
"imap jj <Esc>
"map <A-j> 10<C-e>
"map <A-k> 10<C-y>

" Configure code folding
set foldmethod=manual
"set nofoldenable
" inoremap <leader>f <C-O>za
" nnoremap <leader>f za
" onoremap <leader>f <C-C>za
" vnoremap <leader>f zf
" inoremap <leader>F <C-O>zA
" nnoremap <leader>F zA
" onoremap <leader>F <C-C>zA
" vnoremap <leader>F zf

" Spell check
autocmd BufRead,BufNewFile *.md setlocal spell spelllang=en_us,de
autocmd FileType gitcommit setlocal spell spelllang=en_us,de
" toggle spelling
map <Leader>s :set spell!<CR><Bar>:echo "Spell Check: " . strpart("OffOn", 3 * &spell, 3)<CR>

" set standart indentian
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab autoindent textwidth=220
" autocmd group setting identation by filetype
augroup vimrc
  autocmd!
  autocmd FileType py setlocal              ts=4 sts=4 sw=4 expandtab
  autocmd FileType make set                      sts=0 sw=8 noexpandtab
augroup END

"Performance
set ttyfast
set lazyredraw
set re=1
" ----------------------

" ### Plugins ###

" VimMarkdown
let g:vim_markdown_folding_disabled = 1

" NerdTree
nmap <c-n> :NERDTreeToggle<cr>
" Close when nerdtree is the last window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
let g:plug_window = 'noautocmd vertical topleft new'
let NERDTreeShowHidden=1

" RuboCop stuff
let g:vimrubocop_keymap = 0
nmap <Leader>r :RuboCop<CR>

" Load Nginx Syntax
au BufRead,BufNewFile /etc/nginx/*,/usr/local/nginx/conf/* if &ft == '' | setfiletype nginx | endif

" Ansible
au BufRead,BufNewFile */playbooks/*.yml set filetype=yaml.ansible
let g:ansible_unindent_after_newline = 1
let g:ansible_name_highlight = 'b'
let g:ansible_attribute_highlight = "ab"
let g:ansible_extra_keywords_highlight = 1
let g:ansible_goto_role_paths = './roles,../_common/roles'

" add ruby debugger startpoint to code
nmap <leader>b Obinding.pry<ESC>:w<CR>

"Gutter toggle
" noremap  <Leader>g :GitGutterToggle<CR>

" YouCompleteMe Lsp
"source $HOME/.local/share/site/nvim/lsp-examples/vimrc.generated
"let g:ycm_autoclose_preview_window_after_completion = 1

" make YCM compatible with UltiSnips (using supertab)
"let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
"let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
"let g:SuperTabDefaultCompletionType = '<C-n>'

" better key bindings for UltiSnipsExpandTrigger
" let g:UltiSnipsExpandTrigger = "<tab>"
" let g:UltiSnipsJumpForwardTrigger = "<tab>"
" let g:UltiSnipsJumpBackwardTrigger = "<s-tab>"

" ALE
let g:ale_fixers = {
      \   'javascript': ['eslint'],
      \   'ruby': ['rubocop']
      \}
let g:ale_linters = {
  \   'ruby': ['rubocop'],
  \   'python': ['flake8'],
  \   'ansible': ['ansible-lint'],
  \   'yaml': ['yamllint'],
  \   'ansible_hosts': ['ansible-lint'],
  \   'yaml.ansible': ['ansible-lint'],
  \   'ansible_template': ['ansible-lint']
  \}
let g:ale_lint_on_txt_changed = 0
" let g:ale_sign_column_always = 1

" FZF

 " An action can be a reference to a function that processes selected lines
 function! s:build_quickfix_list(lines)
   call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
   copen
   cc
 endfunction


let g:fzf_action = {
  \ 'ctrl-q': function('s:build_quickfix_list'),
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" Default fzf layout
let g:fzf_layout = { 'down': '50%' }

" Customize fzf colors to match your color scheme
" - fzf#wrap translates this to a set of `--color` options
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" Enable per-command history
" - History files will be stored in the specified directory
" - When set, CTRL-N and CTRL-P will be bound to 'next-history' and
"   'previous-history' instead of 'down' and 'up'.
let g:fzf_history_dir = '~/.local/share/fzf-history'
nnoremap <silent> <C-p> :Files!<CR>
nnoremap <silent> <C-f> :Ag!<CR>
nnoremap <silent> <Leader>f :BLines<CR>
nnoremap <silent> <Leader>F :Lines!<CR>

let g:coc_filetype_map = {
  \ 'yaml.ansible': 'ansible',
  \ }

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file.
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1):
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
